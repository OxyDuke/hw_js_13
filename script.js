const images = document.querySelectorAll(".image-to-show");
let currentIndex = 0;
let timer;
const fadeDuration = 500;

function showNextImage() {
  const currentImage = images[currentIndex];
  currentImage.style.opacity = 0;
  setTimeout(() => {
    currentImage.style.display = "none";
    currentIndex = (currentIndex + 1) % images.length;
    const nextImage = images[currentIndex];
    nextImage.style.display = "block";
    setTimeout(() => {
      nextImage.style.opacity = 1;
    }, 10);
  }, fadeDuration);
}

function startSlideshow() {
  showNextImage();
  timer = setInterval(() => {
    showNextImage();
    const timerElement = document.getElementById("timer");
    timerElement.textContent = `Час до наступного показу: 3 сек`;
  }, 3000);
}

function stopSlideshow() {
  clearInterval(timer);
  const timerElement = document.getElementById("timer");
  timerElement.textContent = "Припинено";
}

document.getElementById("startButton").addEventListener("click", () => {
  startSlideshow();
  document.getElementById("startButton").style.display = "none";
  document.getElementById("stopButton").style.display = "block";
});

document.getElementById("stopButton").addEventListener("click", () => {
  stopSlideshow();
  document.getElementById("stopButton").style.display = "none";
  document.getElementById("resumeButton").style.display = "block";
});

document.getElementById("resumeButton").addEventListener("click", () => {
  startSlideshow();
  document.getElementById("resumeButton").style.display = "none";
  document.getElementById("stopButton").style.display = "block";
});
